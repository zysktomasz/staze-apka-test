package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.controller.dto.participant.ParticipantCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.participant.ParticipantResponseDto;
import com.hibernatemapping.hibernatemapping.entity.Event;
import com.hibernatemapping.hibernatemapping.entity.Participant;
import com.hibernatemapping.hibernatemapping.entity.ApplicationUser;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ParticipantService {
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final EventService eventService;
    private final ParticipantRepository participantRepository;

    public List<Participant> findAll() {
        return participantRepository.findAll();
    }
    public ParticipantResponseDto findById(Integer id) throws ResourceNotFoundException {
        Optional<Participant> participant = participantRepository.findById(id);
        if (!participant.isPresent()){
            throw new ResourceNotFoundException(Participant.class, id);
        }
        return modelMapper.map(participant.get(), ParticipantResponseDto.class);
    }
    public List<ParticipantResponseDto> findByEventId(Integer id) throws ResourceNotFoundException {
        List<Participant> participants = participantRepository.findAllByEventId(id);
        return participants.stream().map(participant -> modelMapper.map(participant, ParticipantResponseDto.class))
                .collect(Collectors.toList());
    }

    public ParticipantResponseDto createParticipant(ParticipantCreateDto participantCreateDto) throws ResourceNotFoundException {
        ApplicationUser user = userService.findById(participantCreateDto.getUserId());
        Event event = eventService.findById(participantCreateDto.getEventId());

        Participant participantToAdd = new Participant(null, event, user, participantCreateDto.getStatus());

        Participant createdParticipant = participantRepository.save(participantToAdd);

        return modelMapper.map(createdParticipant, ParticipantResponseDto.class);
    }
    public void deleteById(Integer id) throws ResourceNotFoundException {
        if (!participantRepository.findById(id).isPresent()) {
            throw new ResourceNotFoundException(Integer.class, id);
        }
        participantRepository.deleteById(id);
    }
    private void entityToResponseDtoTypeMap() {
        if (modelMapper.getTypeMap(Participant.class, ParticipantResponseDto.class) == null) {
            TypeMap<Participant, ParticipantResponseDto> typeMap = modelMapper.createTypeMap(Participant.class, ParticipantResponseDto.class);
            typeMap.addMappings(mapper -> {
                mapper.map(src -> src.getEvent().getId(), ParticipantResponseDto::setEventId);
                mapper.map(src -> src.getApplicationUser().getId(), ParticipantResponseDto::setUserId);
            });
        }
    }
}
