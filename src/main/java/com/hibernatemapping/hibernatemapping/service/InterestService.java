package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.controller.dto.interest.InterestCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.interest.InterestResponseDto;
import com.hibernatemapping.hibernatemapping.entity.Interest;
import com.hibernatemapping.hibernatemapping.entity.Tag;
import com.hibernatemapping.hibernatemapping.entity.ApplicationUser;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.repository.InterestRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
//@RequiredArgsConstructor
public class InterestService {
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final TagService tagService;
    private final InterestRepository interestRepository;

    public InterestService(ModelMapper modelMapper, UserService userService, TagService tagService, InterestRepository interestRepository) {
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.tagService = tagService;
        this.interestRepository = interestRepository;
        // creates TypeMap
        this.entityToResponseDtoTypeMap();
    }

    public InterestResponseDto findById(Integer id) throws ResourceNotFoundException {
        Optional<Interest> interest = interestRepository.findById(id);
        if (!interest.isPresent()) {
            throw new ResourceNotFoundException(Interest.class, id);
        }

        return modelMapper.map(interest.get(), InterestResponseDto.class);
    }

    public List<InterestResponseDto> findInterestsByUser(Integer userId) {
        // TODO: 26-Jun-19 throw exception if applicationUser doesn't exist?
        List<Interest> interests = interestRepository.findAllByApplicationUserId(userId);

        return interests.stream()
                .map(interest -> modelMapper.map(interest, InterestResponseDto.class))
                .collect(Collectors.toList());
    }

    public InterestResponseDto createInterest(InterestCreateDto interestCreateDto) throws ResourceNotFoundException {
        // TODO: 26-Jun-19 could this logic be moved to typemap? (as a converter?)
        ApplicationUser applicationUser = userService.findById(interestCreateDto.getUserId());
        Tag tag = tagService.findById(interestCreateDto.getTagId());
        Interest interestToAdd = new Interest(null, applicationUser, tag);

        Interest createdInterest = interestRepository.save(interestToAdd);

        return modelMapper.map(createdInterest, InterestResponseDto.class);
    }

    public void deleteById(Integer id) throws ResourceNotFoundException {
        if (!interestRepository.findById(id).isPresent()) {
            throw new ResourceNotFoundException(Integer.class, id);
        }
        interestRepository.deleteById(id);
    }

    private void entityToResponseDtoTypeMap() {
        if (modelMapper.getTypeMap(Interest.class, InterestResponseDto.class) == null) {
            TypeMap<Interest, InterestResponseDto> typeMap = modelMapper.createTypeMap(Interest.class, InterestResponseDto.class);
            typeMap.addMappings(mapper -> {
                mapper.map(src -> src.getApplicationUser().getId(), InterestResponseDto::setUserId);
                mapper.map(src -> src.getApplicationUser().getAccountName(), InterestResponseDto::setUserAccountName);
                mapper.map(src -> src.getTag().getId(), InterestResponseDto::setTagId);
                mapper.map(src -> src.getTag().getName(), InterestResponseDto::setTagName);
            });
        }
    }
}
