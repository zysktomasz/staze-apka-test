package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.entity.Tag;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotUniqueException;
import com.hibernatemapping.hibernatemapping.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TagService {
    private final TagRepository tagRepository;

    public List<Tag> findAll() {
        return tagRepository.findAll();
    }
    public List<Tag> findAllByOrderById() {
        return tagRepository.findAllByOrderById();
    }

    public Tag findById(Integer id) throws ResourceNotFoundException {
        Optional<Tag> tag = tagRepository.findById(id);
        if (!tag.isPresent()) {
            throw new ResourceNotFoundException(Tag.class, id);
        }
        return tag.get();
    }

    public Tag save(Tag model) throws ResourceNotUniqueException {
        Tag tag = tagRepository.findByName(model.getName());
        if (tag != null) {
            throw new ResourceNotUniqueException(Tag.class, "name", model.getName());
        }
        return tagRepository.save(model);
    }

    public void deleteById(Integer id) throws ResourceNotFoundException {
        if (!tagRepository.findById(id).isPresent()) {
            throw new ResourceNotFoundException(Tag.class, id);
        }
        tagRepository.deleteById(id);
    }

    public Tag update(Integer id, Tag model) throws ResourceNotFoundException {
        Optional<Tag> tag = tagRepository.findById(id);
        if (!tag.isPresent()) {
            throw new ResourceNotFoundException(Tag.class, id);
        }

        Tag tagToUpdate = tag.get();
        tagToUpdate.setName(model.getName());
        tagRepository.save(tagToUpdate);

        return tagToUpdate;
    }
}
