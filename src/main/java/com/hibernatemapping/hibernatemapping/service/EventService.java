package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.controller.dto.event.EventCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.event.EventResponseDto;
import com.hibernatemapping.hibernatemapping.entity.ApplicationUser;
import com.hibernatemapping.hibernatemapping.entity.City;
import com.hibernatemapping.hibernatemapping.entity.Event;
import com.hibernatemapping.hibernatemapping.exception.PeopleAmountsDontMatchException;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.exception.StartDateAfterEndDateException;
import com.hibernatemapping.hibernatemapping.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventService {
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final CityService cityService;
    private final EventRepository eventRepository;

    public List<Event> findAll(){
        return eventRepository.findAll();
    }

    public Event findById(Integer id) throws ResourceNotFoundException {
        Optional<Event> event = eventRepository.findById(id);
        if (!event.isPresent()) {
            throw new ResourceNotFoundException(Event.class, id);
        }
        return event.get();
    }

    public List<EventResponseDto> findAllByParticipants(Integer userId){
        List<Event> events = eventRepository.findAllByParticipants(userId);
        return events.stream().map(event -> modelMapper.map(event, EventResponseDto.class)).collect(Collectors.toList());
    }
    public EventResponseDto createEvent(EventCreateDto eventCreateDto) throws ResourceNotFoundException, StartDateAfterEndDateException, PeopleAmountsDontMatchException {
        ApplicationUser user = userService.findById(eventCreateDto.getEventCreatorId());
        City city = cityService.findById(eventCreateDto.getCityId());

        if (eventCreateDto.getStartDate().isAfter(eventCreateDto.getEndDate())
                || eventCreateDto.getStartDate().equals(eventCreateDto.getEndDate())) {
            throw new StartDateAfterEndDateException(EventCreateDto.class);
        }

        if (eventCreateDto.getMinPeopleAmount() > eventCreateDto.getMaxPeopleAmount()
            || eventCreateDto.getMinPeopleAmount() < 0
            || eventCreateDto.getMaxPeopleAmount() < 0) {
            throw new PeopleAmountsDontMatchException(EventCreateDto.class);
        }

        Event eventToAdd = new Event(null, eventCreateDto.getName(), user, eventCreateDto.getStartDate(),
                    eventCreateDto.getEndDate(), eventCreateDto.getMinPeopleAmount(), eventCreateDto.getMaxPeopleAmount(),
                    city, eventCreateDto.getPlace(), eventCreateDto.getDescription(), eventCreateDto.getIsPublic());
        Event createdEvent = eventRepository.save(eventToAdd);



//        TypeMap<Event, EventResponseDto> typeMap = modelMapper.createTypeMap(Event.class, EventResponseDto.class);
//        typeMap.addMappings(mapper -> {
//            mapper.map(src -> src.getEventCreator().getId(), EventResponseDto::setEventCreatorId);
//            mapper.map(src -> src.getCity().getId(), EventResponseDto::setCityId);
//        });

        return modelMapper.map(createdEvent, EventResponseDto.class);
    }
    public void deleteById(Integer id) throws ResourceNotFoundException {
        if (!eventRepository.findById(id).isPresent()) {
            throw new ResourceNotFoundException(Integer.class, id);
        }
        eventRepository.deleteById(id);
    }
    private void entityToResponseDtoTypeMap() {
        if (modelMapper.getTypeMap(Event.class, EventResponseDto.class) == null) {
            TypeMap<Event, EventResponseDto> typeMap = modelMapper.createTypeMap(Event.class, EventResponseDto.class);
            typeMap.addMappings(mapper -> {
                mapper.map(src -> src.getEventCreator().getId(), EventResponseDto::setEventCreatorId);
                mapper.map(src -> src.getCity().getId(), EventResponseDto::setCityId);
            });
        }
    }
}
