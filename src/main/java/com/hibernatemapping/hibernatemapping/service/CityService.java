package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.entity.City;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.repository.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CityService {
    private final CityRepository cityRepository;

    public List<City> findAll() {
        return cityRepository.findAll();
    }

//    public Optional<City> findById(Integer id) {
//        return cityRepository.findById(id);
//    }
    public City findById(Integer id) throws ResourceNotFoundException {
        Optional<City> city = cityRepository.findById(id);
        if (!city.isPresent()) {
            throw new ResourceNotFoundException(City.class, id);
        }

        return city.get();
    }
    // TODO: 21-Jun-19 sprawdzac czy unikalna nazwa(?)
    public City save(City model) {
        return cityRepository.save(model);
    }

    public void deleteById(Integer id) throws ResourceNotFoundException {
        Optional<City> city = cityRepository.findById(id);
        if (!city.isPresent()) {
            throw new ResourceNotFoundException(City.class, id);
        }

        cityRepository.deleteById(id);
    }

    public Optional<City> update(Integer id, City model) {
        Optional<City> city = cityRepository.findById(id);
        if (!city.isPresent()) {
            return city;
        }

        City updatedCity = city.get();
        updatedCity.setName(model.getName());
        cityRepository.save(updatedCity);

        return Optional.of(updatedCity);
    }
}
