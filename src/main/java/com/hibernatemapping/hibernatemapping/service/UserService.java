package com.hibernatemapping.hibernatemapping.service;

import com.hibernatemapping.hibernatemapping.entity.ApplicationUser;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.repository.ApplicationUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final ApplicationUserRepository applicationUserRepository;

    public ApplicationUser findById(Integer id) throws ResourceNotFoundException {
        Optional<ApplicationUser> user = applicationUserRepository.findById(id);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException(ApplicationUser.class, id);
        }

        return user.get();
    }

}
