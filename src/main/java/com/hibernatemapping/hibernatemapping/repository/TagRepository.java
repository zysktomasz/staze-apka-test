package com.hibernatemapping.hibernatemapping.repository;

import com.hibernatemapping.hibernatemapping.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    List<Tag> findAllByOrderById();
    Tag findByName(String name);
}
