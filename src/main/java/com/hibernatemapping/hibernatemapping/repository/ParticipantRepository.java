package com.hibernatemapping.hibernatemapping.repository;

import com.hibernatemapping.hibernatemapping.entity.Event;
import com.hibernatemapping.hibernatemapping.entity.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParticipantRepository  extends JpaRepository<Participant, Integer> {
    List<Participant> findAllByEventId(Integer eventId);
}
