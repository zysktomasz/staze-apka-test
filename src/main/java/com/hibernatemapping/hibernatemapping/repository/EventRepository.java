package com.hibernatemapping.hibernatemapping.repository;

import com.hibernatemapping.hibernatemapping.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Integer> {
//    @Query("SELECT e from Event e, Participant p, ApplicationUser u where e = p.event and p.user = u and u.id = :userId")
    @Query("SELECT e from Event e, Participant p, ApplicationUser u where e = p.event and p.applicationUser = u and u.id = :userId")
    List<Event> findAllByParticipants(Integer userId);
}
