package com.hibernatemapping.hibernatemapping.repository;

import com.hibernatemapping.hibernatemapping.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository  extends JpaRepository<City, Integer> {
}

