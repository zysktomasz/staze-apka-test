package com.hibernatemapping.hibernatemapping.repository;

import com.hibernatemapping.hibernatemapping.entity.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Integer> {
    ApplicationUser findByEmail(String email);
}

