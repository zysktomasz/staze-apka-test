package com.hibernatemapping.hibernatemapping.controller;

import com.hibernatemapping.hibernatemapping.controller.dto.event.EventCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.event.EventResponseDto;
import com.hibernatemapping.hibernatemapping.entity.Event;
import com.hibernatemapping.hibernatemapping.exception.PeopleAmountsDontMatchException;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.exception.StartDateAfterEndDateException;
import com.hibernatemapping.hibernatemapping.repository.EventRepository;
import com.hibernatemapping.hibernatemapping.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/events")
public class EventController {
    private final EventService eventService;


    @Autowired
    EventRepository repo;

    @GetMapping("{id}")
    public ResponseEntity<Event> findById(@PathVariable Integer id) throws ResourceNotFoundException {
//        EventResponseDto eventResponseDto = eventService.findById(id);
        Event event = eventService.findById(id);
        return ResponseEntity.ok(event);
    }

    @GetMapping("/user/{userId}")
    public List<EventResponseDto> retrieveEventsByUser(@PathVariable @NotNull Integer userId) {
        repo.findAllByParticipants(userId);
//        return Collections.EMPTY_LIST;
        return eventService.findAllByParticipants(userId);
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody EventCreateDto eventCreateDto) throws ResourceNotFoundException, StartDateAfterEndDateException, PeopleAmountsDontMatchException {
        EventResponseDto createdEventResponseDto = eventService.createEvent(eventCreateDto);
        return ResponseEntity.ok(createdEventResponseDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable @NotNull Integer id) throws ResourceNotFoundException {
        eventService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
