package com.hibernatemapping.hibernatemapping.controller;

import com.hibernatemapping.hibernatemapping.controller.dto.interest.InterestCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.interest.InterestResponseDto;
import com.hibernatemapping.hibernatemapping.error.ApiError;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.service.InterestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/interests")
@Api(value = "Interests ddddd")
public class InterestController {
    private final InterestService interestService;

    @ApiOperation(value = "View a list of interests for specified ApplicationUser", response = List.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list", response = InterestResponseDto.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Validation or service errors", response = ApiError.class),
    })
    @GetMapping("/user/{userId}")
    public List<InterestResponseDto> retrieveInterestsByUser(@PathVariable @NotNull Integer userId) {
        return interestService.findInterestsByUser(userId);
    }

    @ApiOperation(value = "View details about specified Interest", response = InterestResponseDto.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Interest details", response = InterestResponseDto.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Validation or service errors", response = ApiError.class),
    })
    @GetMapping("{id}")
    public ResponseEntity<InterestResponseDto> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        InterestResponseDto interestResponseDto = interestService.findById(id);
        return ResponseEntity.ok(interestResponseDto);
    }

    @ApiOperation(value = "Create new Interest", response = List.class, produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created new Interest", response = InterestResponseDto.class),
            @ApiResponse(code = 400, message = "Validation or service errors", response = ApiError.class),
    })
    @PostMapping
    public ResponseEntity create(@Valid @RequestBody InterestCreateDto interestCreateDto) throws ResourceNotFoundException {
        InterestResponseDto createdInterestResponseDto = interestService.createInterest(interestCreateDto);

        return ResponseEntity.ok(createdInterestResponseDto);
    }

    @ApiOperation(value = "Remove specified Interest")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Successfully removed Interest"),
            @ApiResponse(code = 400, message = "Validation or service errors", response = ApiError.class),
    })
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable @NotNull Integer id) throws ResourceNotFoundException {
        interestService.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
