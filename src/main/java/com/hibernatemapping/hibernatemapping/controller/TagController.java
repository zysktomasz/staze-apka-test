package com.hibernatemapping.hibernatemapping.controller;

import com.hibernatemapping.hibernatemapping.controller.dto.tag.TagResponseDto;
import com.hibernatemapping.hibernatemapping.controller.dto.tag.TagSaveDto;
import com.hibernatemapping.hibernatemapping.entity.Tag;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotUniqueException;
import com.hibernatemapping.hibernatemapping.service.TagService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/tags")
public class TagController {
    private final TagService tagService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<TagResponseDto>> findAll() {
        List<Tag> tags = tagService.findAllByOrderById();

        List<TagResponseDto> tagsDto = tags.stream()
                .map(tag -> modelMapper.map(tag, TagResponseDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(tagsDto);
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody TagSaveDto tagSaveDto) throws ResourceNotUniqueException {
        Tag tagToSave = modelMapper.map(tagSaveDto, Tag.class);
        Tag savedTag = tagService.save(tagToSave);
        TagResponseDto savedTagDto = modelMapper.map(savedTag, TagResponseDto.class);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedTagDto.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagResponseDto> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        Tag tag = tagService.findById(id);
        TagResponseDto tagReturnDto = modelMapper.map(tag, TagResponseDto.class);

        return ResponseEntity.ok(tagReturnDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TagResponseDto> update(@PathVariable Integer id, @Valid @RequestBody TagSaveDto tagUpdateDto) throws ResourceNotFoundException {
        Tag tagToUpdate = modelMapper.map(tagUpdateDto, Tag.class);
        Tag updatedTag = tagService.update(id, tagToUpdate);

        TagResponseDto updatedTagDto = modelMapper.map(updatedTag, TagResponseDto.class);

        return ResponseEntity.ok(updatedTagDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) throws ResourceNotFoundException {
        tagService.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
