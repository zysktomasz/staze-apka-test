package com.hibernatemapping.hibernatemapping.controller;

import com.hibernatemapping.hibernatemapping.controller.dto.city.CityCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.city.CityResponseDto;
import com.hibernatemapping.hibernatemapping.controller.dto.city.CityUpdateDto;
import com.hibernatemapping.hibernatemapping.entity.City;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.service.CityService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/cities")
@RequiredArgsConstructor
public class CityController {
    // TODO: 21-Jun-19 autoryzacja administratora dla operacji POST, PUT, DELETE
    // TODO: 21-Jun-19 konfiguracja endpointów api
    private final CityService cityService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<CityResponseDto>> findAll() {
        List<City> cities = cityService.findAll();

        List<CityResponseDto> citiesDto = cities.stream()
                .map(city -> modelMapper.map(city, CityResponseDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(citiesDto);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "link do nowego obiektu"),
    })
    @PostMapping
    public ResponseEntity create(@Valid @RequestBody CityCreateDto cityCreateDto) {
        City cityToSave = modelMapper.map(cityCreateDto, City.class);
        City savedCity = cityService.save(cityToSave);
        CityResponseDto savedCityDto = modelMapper.map(savedCity, CityResponseDto.class);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedCityDto.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CityResponseDto> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        City city = cityService.findById(id);

        CityResponseDto cityReturnDto = modelMapper.map(city, CityResponseDto.class);

        return ResponseEntity.ok(cityReturnDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CityResponseDto> update(@PathVariable Integer id, @Valid @RequestBody CityUpdateDto cityUpdateDto) {
        City cityToUpdate = modelMapper.map(cityUpdateDto, City.class);
        Optional<City> updatedCity = cityService.update(id, cityToUpdate);
        if (!updatedCity.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        CityResponseDto updatedCityDto = modelMapper.map(updatedCity.get(), CityResponseDto.class);

        return ResponseEntity.ok(updatedCityDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Integer id) throws ResourceNotFoundException {
        cityService.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
