package com.hibernatemapping.hibernatemapping.controller;

import com.hibernatemapping.hibernatemapping.controller.dto.participant.ParticipantCreateDto;
import com.hibernatemapping.hibernatemapping.controller.dto.participant.ParticipantResponseDto;
import com.hibernatemapping.hibernatemapping.exception.ResourceNotFoundException;
import com.hibernatemapping.hibernatemapping.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/participants")
public class ParticipantController {
    private final ParticipantService participantService;

    @GetMapping("{id}")
    public ResponseEntity<ParticipantResponseDto> findById(@PathVariable Integer id) throws ResourceNotFoundException {
        ParticipantResponseDto participantResponseDto = participantService.findById(id);
        return ResponseEntity.ok(participantResponseDto);
    }
    @GetMapping("/event/{eventId}")
    public List<ParticipantResponseDto> retrieveByEventId(@PathVariable @NotNull Integer eventId) throws ResourceNotFoundException {
        return participantService.findByEventId(eventId);
    }
    @PostMapping
    public ResponseEntity create(@Valid @RequestBody ParticipantCreateDto participantCreateDto) throws ResourceNotFoundException {
        ParticipantResponseDto createdEventResponseDto = participantService.createParticipant(participantCreateDto);
        return ResponseEntity.ok(createdEventResponseDto);
    }
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable @NotNull Integer id) throws ResourceNotFoundException {
        participantService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
