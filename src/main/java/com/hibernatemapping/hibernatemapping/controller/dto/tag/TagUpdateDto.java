package com.hibernatemapping.hibernatemapping.controller.dto.tag;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class TagUpdateDto {
    @NotBlank
    private String name;
}
