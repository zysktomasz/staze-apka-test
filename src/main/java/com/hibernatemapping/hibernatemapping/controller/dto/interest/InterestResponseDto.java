package com.hibernatemapping.hibernatemapping.controller.dto.interest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InterestResponseDto {
    private Integer id;

    private Integer userId;

    private String userAccountName;

    private Integer tagId;

    private String tagName;
}