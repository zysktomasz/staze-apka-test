package com.hibernatemapping.hibernatemapping.controller.dto.event;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
@Setter
public class EventUpdateDto {
    @NotBlank
    private String name;

    @NotNull
    private Integer eventCreatorId;

    @NotNull
    private Instant startDate;

    private Instant endDate;

    private Integer minPeopleAmount;

    private Integer maxPeopleAmount;

    @NotNull
    private Integer cityId;

    @NotBlank
    private String place;

    private String description;

    private Boolean isPublic;
}
