package com.hibernatemapping.hibernatemapping.controller.dto.city;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityResponseDto {
    private Integer id;

    private String name;
}