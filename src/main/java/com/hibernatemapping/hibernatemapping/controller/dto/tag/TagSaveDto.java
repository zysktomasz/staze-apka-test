package com.hibernatemapping.hibernatemapping.controller.dto.tag;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

// SaveDto - jesli jest model taki sam do tworzenia i do edycji (Create i Update)
// podzial na CreateDto i UpdateDto - jesli się roznia
// DO PRZEMYSLENIA
// DO PRZEMYSLENIA
// DO PRZEMYSLENIA
@Getter
@Setter
public class TagSaveDto {
    @NotBlank
    private String name;
}
