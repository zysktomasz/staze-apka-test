package com.hibernatemapping.hibernatemapping.controller.dto.participant;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ParticipantCreateDto {
    @NotNull
    private Integer eventId;

    @NotNull
    private Integer userId;

    private String status;
}
