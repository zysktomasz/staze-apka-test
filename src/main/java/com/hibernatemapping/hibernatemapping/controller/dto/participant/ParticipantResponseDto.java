package com.hibernatemapping.hibernatemapping.controller.dto.participant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParticipantResponseDto {
    private Integer id;
    private Integer eventId;
    private Integer userId;
    private String status;
}
