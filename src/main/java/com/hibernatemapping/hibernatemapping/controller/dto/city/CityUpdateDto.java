package com.hibernatemapping.hibernatemapping.controller.dto.city;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class CityUpdateDto {
    @NotBlank
    private String name;
}