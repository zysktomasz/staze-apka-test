package com.hibernatemapping.hibernatemapping.controller.dto.tag;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagResponseDto {
    private Integer id;

    private String name;
}
