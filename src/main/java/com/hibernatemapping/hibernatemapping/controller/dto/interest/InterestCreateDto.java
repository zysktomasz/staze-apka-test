package com.hibernatemapping.hibernatemapping.controller.dto.interest;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class InterestCreateDto {
    // TODO: 25-Jun-19 get userId from logged in applicationUser
    @NotNull
    private Integer userId;
    @NotNull
    private Integer tagId;
}