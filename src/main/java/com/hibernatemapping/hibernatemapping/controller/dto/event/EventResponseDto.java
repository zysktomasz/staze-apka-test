package com.hibernatemapping.hibernatemapping.controller.dto.event;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class EventResponseDto {
    private Integer id;
    private String name;
    private Integer eventCreatorId;
    private Instant startDate;
    private Instant endDate;
    private Integer minPeopleAmount;
    private Integer maxPeopleAmount;
    private Integer cityId;
    private String place;
    private String description;
    private Boolean isPublic;
}
