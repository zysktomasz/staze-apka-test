package com.hibernatemapping.hibernatemapping.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "event_creator_id", nullable = false)
    private ApplicationUser eventCreator;

    @NotNull
    private Instant startDate;

    @NotNull
    private Instant endDate;

//    @Column(name = "minpeopleamount")
    private Integer minPeopleAmount;

//    @Column(name = "maxpeopleamount")
    private Integer maxPeopleAmount;

    @ManyToOne
    @JoinColumn(name = "city_id", nullable = false)
    private City city;

    private String place;

    private String description;

//    @Column(name = "ispublic")
    private Boolean isPublic;

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
//    private Set<Participant> participants = new HashSet<>();
}
