package com.hibernatemapping.hibernatemapping.exception;

public class ResourceNotUniqueException extends Exception {
    public ResourceNotUniqueException(Class clazz, String propertyName, String propertyValue) {
        super(clazz.getSimpleName() + " entity already exists for " + propertyName + " = " + propertyValue);
    }
}
