package com.hibernatemapping.hibernatemapping.exception;

// TODO: 6/25/2019 should extend Exception or RuntimeException ??
// TODO: 26-Jun-19 more generic approach (if exception returned based on different props than id
public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(Class clazz, Integer id) {
        super(clazz.getSimpleName() + " entity was not found for id: " + id);
    }
}
