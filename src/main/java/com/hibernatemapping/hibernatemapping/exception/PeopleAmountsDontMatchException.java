package com.hibernatemapping.hibernatemapping.exception;

public class PeopleAmountsDontMatchException extends Exception {
    public PeopleAmountsDontMatchException(Class clazz) {
        super(clazz.getSimpleName() + " min and max people amounts don't match ");
    }
}