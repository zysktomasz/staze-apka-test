package com.hibernatemapping.hibernatemapping.exception;

public class StartDateAfterEndDateException extends Exception {
    public StartDateAfterEndDateException(Class clazz){
        super(clazz.getSimpleName() + " start date and end date don't match");
    }
}
